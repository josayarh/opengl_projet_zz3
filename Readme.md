# Pr�requis 
SFML
Compilateur compatible C++17
Cmake 3.7
GLUT

# Execution
Compilation avec le Cmake (test� avec Clion) disponible en racine du projet puis ex�cution du code dans le dossier build

# Objectifs accomplis 

Affichage d�une sc�ne 3D contenant plusieurs objets
D�placement de la cam�ra dans la sc�ne (WASD pour d�placement, click droit de la souris pour changer d'angle)
Eclairage Ambiant
Eclairage Diffus et / ou sp�culaire sur certains objets

Programmation par Shaders et VBO

# Explication de l'impl�mentation 

Pour ce projet je suis parti de zero car je n'ai pas pu utiliser la base de mr malgouyres sous windows. Voici donc le projet Cmake qui regroupe mon travail pour ce projet. Les classes sont divis�e en Mod�le, View et Controlleur pour respecter l'architecture d'un MVS comme demand� par mr malgouyres. 

## Controlleur 
La classe controlleur se charge de la cr�ation du mod�le et de la fen�tre et de la gestion des �venements qu'il transmet par la suite aux �l�ments appropri�s. 

## View 
H�rite de la classe SFML sf::Window dispose d'une m�thode show qui affiche le contenu de la fen�tre toutes les secondes, stocke �galement les matrices projections et vue. 

## Mod�le 
Classe contenant la cam�ra, les objets 3d a mod�liser, et une classe qui permet de compter le nombre de frame par seconde. Cr�e les objets 3d et leur matrice de model.  

## DDDObject
Mod�lise les objets 3d en eux m�me en contenant les positions des vertices, les couleurs, les vecteurs normaux (qui sont la pour la gestion de la lumi�re), le shader, la matrice de model, les vbo ainsi que le vao. S'occupe de la cr�ation de l'objet 3d. Permet de g�rer n'importe quel objet 3D tant que l'on dispose des bonne donn�es. 

## Shader 
Prends en entr�e le chemin d'un vertex shader et un fragment shader et ex�cuite les opp�rations de cr�ation de celui ci. Classe prise de LearnOpenGl.com et modifi�e au fur a mesure de l'avenc�e sur le projet. 

## FrameData
Classe de mesure des FPS. 

## Camera
Classe de gestion de la cam�ra et tout ses mouvements

## Shader_base.frag
G�re la couleur et la lumi�re sur les objets 3d

## Shader_base.vert 
G�re la position des objets 3d, et passe les donn�es n�cessaires a Shader_base.frag ou Shaderlight.frag

## Shader_light.frag
G�re la couleur blanche du cube qui symbolise la source de lumi�re 

# Sc�ne et explications
La sc�ne actuelle dispose de 3 cubes, deux multicolors ayant les effets de lumi�re et 1 blanc qui sert de source de lumi�re. Il est possible de de d�placer dans la sc�ne via la cam�ra. L'utilisation des DDDobjects rends assez simple la cr�ation de nouveaux objets 3D et, je l'esp�re, permet d'avoir un d�couplage important entre les diff�rentes classes du projet. 
