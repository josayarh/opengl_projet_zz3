var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz",
  1: "cdfgmstv",
  2: "cdfgkmsv",
  3: "cdghimorstuv",
  4: "bcefgimnprstv",
  5: "abcdefghijklmnopqrstuvwxyz",
  6: "k",
  7: "ks",
  8: "_acdghkps"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

