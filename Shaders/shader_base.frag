#version 330 core
 out vec4 FragColor;
 in vec4 Color;
 in vec3 Normal;
 in vec3 FragPos;

 uniform vec4 lightColor;
 uniform vec4 lightPos;
 uniform vec3 viewPos;

 void main()
 {
    //Calcul de la lumière ambiente
    float ambientStrengh = 0.1;
    vec4 ambientLight = lightColor * ambientStrengh;
    ambientLight.w = 1;

    //Calcul de la lumière diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(vec3(lightPos.x,lightPos.y,lightPos.z) - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec4 diffuse = diff * lightColor;

    //Calcul de la lumière spéculaire
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * vec3(lightPos.x,lightPos.y,lightPos.z);

    vec4 result = (ambientLight + diffuse + vec4(specular,1)) * Color;
    result.w=1;
    FragColor = result;
 }