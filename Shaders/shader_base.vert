#version 330 core
layout(location = 0) in vec3 aPos; // the position variable has attribute position 0
layout(location = 1) in vec4 vertexColor;
layout(location = 2) in vec3 normal;

out vec4 Color; // specify a color output to the fragment shader
out vec3 Normal;
out vec3 FragPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(aPos, 1.0); // see how we directly give a vec3 to vec4's constructor
    FragPos = vec3(model * vec4(aPos, 1.0));
    Color = vertexColor;
    Normal = mat3(transpose(inverse(model))) * normal;
}