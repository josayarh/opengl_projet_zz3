#include <iostream>
#include <SFML/Window.hpp>

#include <Controlleur.hpp>

int main() {
    sf::ContextSettings settings;

    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 4;

    Controlleur control(settings);

    while(control.run());
    return 0;
}