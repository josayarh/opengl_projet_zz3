//
// Created by jojoX on 19/12/2018.
//

#ifndef PROJET_OPEN_GL_CAMERA_HPP
#define PROJET_OPEN_GL_CAMERA_HPP


#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/System.hpp>

class Camera {

    glm::vec3 position;
    glm::vec3 target;
    glm::vec3 up;
    glm::mat4 view;
    sf::Clock clock;
    sf::Vector2i lastMousePos;
    bool isFirstClick = true;

    float yaw, pitch;

public:
    Camera(glm::vec3 position, glm::vec3 target, glm::vec3 up);
    void moveForward(float speed);
    void moveBackward(float speed);
    void moveRight(float speed);
    void moveLeft(float speed);
    void testUpdate();
    void handleMouseMouvement(sf::Vector2i& mousePosition);

    void setIsFirstClick(bool isFirstClick);

    const glm::mat4 &getView();

    const glm::vec3 &getPosition() const;
};


#endif //PROJET_OPEN_GL_CAMERA_HPP
