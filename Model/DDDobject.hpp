//
// Created by jojoX on 19/12/2018.
//

#ifndef PROJET_OPEN_GL_DDDOBJECT_HPP
#define PROJET_OPEN_GL_DDDOBJECT_HPP


#include <glm/vec3.hpp>
#include <vector>
#include <array>
#include <glm/mat4x4.hpp>

#include "Shader.hpp"

class DDDobject {
private:
    std::vector<float> vertices;
    std::vector<float> colors;
    std::vector<float> normals;
    std::vector<float> textureUV;

    unsigned int vbo[3], vao;
    Shader shader;

    void bind3D();
public:
    glm::mat4 modelMatrix;


    DDDobject(std::initializer_list<float> vertices, std::initializer_list<float> colors, std::string vertexShader, std::string FragmentShader)
    :vertices(vertices), colors(colors),shader(vertexShader.c_str(), FragmentShader.c_str())
    {
        bind3D();
    }
    DDDobject(std::initializer_list<float> vertices, std::initializer_list<float> colors, std::initializer_list<float> normals, std::string vertexShader, std::string FragmentShader)
    :vertices(vertices), colors(colors), normals(normals),shader(vertexShader.c_str(), FragmentShader.c_str())
    {
        bind3D();
    }
    DDDobject(std::initializer_list<float> vertices, std::initializer_list<float> colors, std::initializer_list<float> normals, std::vector<float> textureUV,
            std::string vertexShader, std::string FragmentShader)
            :vertices(vertices), colors(colors), normals(normals),shader(vertexShader.c_str(), FragmentShader.c_str())
    {
        bind3D();
    }

    DDDobject& operator=(const DDDobject& ddDobject);
    void show() const;

    void useShader(){shader.use();}
    const Shader &getShader() const;
};


#endif //PROJET_OPEN_GL_DDDOBJECT_HPP
