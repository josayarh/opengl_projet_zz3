//
// Created by jojoX on 19/12/2018.
//

#include <glm/gtc/matrix_transform.hpp>
#include <chrono>
#include <cmath>
#include <SFML/Window/Mouse.hpp>

#include "Camera.hpp"

Camera::Camera(glm::vec3 position, glm::vec3 target, glm::vec3 up):position(position),target(target),up(up) {
    view = glm::lookAt(position, position + target, up);
    lastMousePos = sf::Vector2i(0,0);
    clock.restart();
    yaw= -90.0f;
    pitch = 0;
}

void Camera::moveForward(float speed) {
    position += speed * target;
}

void Camera::moveBackward(float speed) {
    moveForward(-speed);
}

void Camera::moveRight(float speed) {
    position += glm::normalize(glm::cross(target,up)) * speed;
}

void Camera::moveLeft(float speed) {
    moveRight(-speed);
}

const glm::mat4 &Camera::getView() {
    view = glm::lookAt(position, position + target, up);
    return view;
}

void Camera::testUpdate() {
    view = glm::mat4(1.0f);
    float radius = 10.0f;
    float camX = std::sin(clock.getElapsedTime().asSeconds()) * radius;
    float camZ = std::cos(clock.getElapsedTime().asSeconds()) * radius;
    view = glm::lookAt(glm::vec3(camX, 0.0, camZ), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0,0.0));
}

void Camera::handleMouseMouvement(sf::Vector2i& mousePosition) {

    if(isFirstClick) {
        lastMousePos = mousePosition;
        isFirstClick = false;
    }

    float xoffset = mousePosition.x - lastMousePos.x;
    float yoffset = lastMousePos.y - mousePosition.y;
    float sensitivity = 0.05f;

    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;
    pitch += yoffset;

    if(pitch > 89.0f)
        pitch =  89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 front;
    front.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    front.y = sin(glm::radians(pitch));
    front.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
    target = glm::normalize(front);

    lastMousePos = mousePosition;
}

void Camera::setIsFirstClick(bool isFirstClick) {
    Camera::isFirstClick = isFirstClick;
}

const glm::vec3 &Camera::getPosition() const {
    return position;
}
