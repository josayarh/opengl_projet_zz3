//
// Created by jojoX on 19/10/2018.
//
#include <glad/glad.h>

#include <iostream>
#include "Model.hpp"
#include "Mesh.hpp"

Model::Model(int height, int length) : height(height), length(length), shader
        ("..\\Shaders\\shader_base.vert",
        "..\\Shaders\\shader_base.frag"),
        camera(glm::vec3(0,0,10.0f), glm::vec3(0,0,-1.0f), glm::vec3(0,2.0f,0))
{

    makeCube2();

    FramesData::Init();
}

void Model::makeTriangle() {
    vaoNumber =2;
    pointsNumber =3;

    float vertices[] = {
            -0.5f, -0.5f, 0.0f,
            0.0f, -0.5f, 0.0f,
            -0.25f,  0.0f, 0.0f
    };

    float vertices2[] = {
            0.0f, 0.5f, 0.0f,
            0.5f, 0.5f, 0.0f,
            0.25f,  0.0f, 0.0f
    };

    glGenVertexArrays(2, vao);
    glGenBuffers(2, vbo);


    glBindVertexArray(vao[0]);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(vao[1]);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

}

void Model::makeCube(){
    vaoNumber =1;
    pointsNumber =12*3;

    GLfloat vertices[] = {
        -1.0f,-1.0f,-1.0f, // triangle 1 : begin
        -1.0f,-1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f, // triangle 1 : end
        1.0f, 1.0f,-1.0f, // triangle 2 : begin
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f, // triangle 2 : end
        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f
    };

    GLfloat g_color_buffer_data[] = {
            0.583f,  0.771f,  0.014f, 1,
            0.609f,  0.115f,  0.436f, 1,
            0.327f,  0.483f,  0.844f, 1,
            0.822f,  0.569f,  0.201f, 1,
            0.435f,  0.602f,  0.223f, 1,
            0.310f,  0.747f,  0.185f, 1,
            0.597f,  0.770f,  0.761f, 1,
            0.559f,  0.436f,  0.730f, 1,
            0.359f,  0.583f,  0.152f, 1,
            0.483f,  0.596f,  0.789f, 1,
            0.559f,  0.861f,  0.639f, 1,
            0.195f,  0.548f,  0.859f, 1,
            0.014f,  0.184f,  0.576f, 1,
            0.771f,  0.328f,  0.970f, 1,
            0.406f,  0.615f,  0.116f, 1,
            0.676f,  0.977f,  0.133f, 1,
            0.971f,  0.572f,  0.833f, 1,
            0.140f,  0.616f,  0.489f, 1,
            0.997f,  0.513f,  0.064f, 1,
            0.945f,  0.719f,  0.592f, 1,
            0.543f,  0.021f,  0.978f, 1,
            0.279f,  0.317f,  0.505f, 1,
            0.167f,  0.620f,  0.077f, 1,
            0.347f,  0.857f,  0.137f, 1,
            0.055f,  0.953f,  0.042f, 1,
            0.714f,  0.505f,  0.345f, 1,
            0.783f,  0.290f,  0.734f, 1,
            0.722f,  0.645f,  0.174f, 1,
            0.302f,  0.455f,  0.848f, 1,
            0.225f,  0.587f,  0.040f, 1,
            0.517f,  0.713f,  0.338f, 1,
            0.053f,  0.959f,  0.120f, 1,
            0.393f,  0.621f,  0.362f, 1,
            0.673f,  0.211f,  0.457f, 1,
            0.820f,  0.883f,  0.371f, 1,
            0.982f,  0.099f,  0.879f, 1
    };

    glGenVertexArrays(1, &vao[0]);
    glGenBuffers(2, vbo);
    glBindVertexArray(vao[0]);

    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_color_buffer_data), g_color_buffer_data, GL_STATIC_DRAW);

    int vertexColorLocation = 1; //glGetUniformLocation(getShaderProgram(), "vertexColor");
    glVertexAttribPointer(vertexColorLocation, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(vertexColorLocation);

}

void Model::makeCube2() {
    DDDobject object (
            {
                    -0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f,  0.5f, -0.5f,
                    0.5f,  0.5f, -0.5f,
                    -0.5f,  0.5f, -0.5f,
                    -0.5f, -0.5f, -0.5f,

                    -0.5f, -0.5f,  0.5f,
                    0.5f, -0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f,  0.5f,
                    -0.5f, -0.5f,  0.5f,

                    -0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f, -0.5f,
                    -0.5f, -0.5f, -0.5f,
                    -0.5f, -0.5f, -0.5f,
                    -0.5f, -0.5f,  0.5f,
                    -0.5f,  0.5f,  0.5f,

                    0.5f,  0.5f,  0.5f,
                    0.5f,  0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,

                    -0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f,  0.5f,
                    0.5f, -0.5f,  0.5f,
                    -0.5f, -0.5f,  0.5f,
                    -0.5f, -0.5f, -0.5f,

                    -0.5f,  0.5f, -0.5f,
                    0.5f,  0.5f, -0.5f,
                    0.5f,  0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f, -0.5f
            },
            {
                    0.583f,  0.771f,  0.014f, 1,
                    0.609f,  0.115f,  0.436f, 1,
                    0.327f,  0.483f,  0.844f, 1,
                    0.822f,  0.569f,  0.201f, 1,
                    0.435f,  0.602f,  0.223f, 1,
                    0.310f,  0.747f,  0.185f, 1,
                    0.597f,  0.770f,  0.761f, 1,
                    0.559f,  0.436f,  0.730f, 1,
                    0.359f,  0.583f,  0.152f, 1,
                    0.483f,  0.596f,  0.789f, 1,
                    0.559f,  0.861f,  0.639f, 1,
                    0.195f,  0.548f,  0.859f, 1,
                    0.014f,  0.184f,  0.576f, 1,
                    0.771f,  0.328f,  0.970f, 1,
                    0.406f,  0.615f,  0.116f, 1,
                    0.676f,  0.977f,  0.133f, 1,
                    0.971f,  0.572f,  0.833f, 1,
                    0.140f,  0.616f,  0.489f, 1,
                    0.997f,  0.513f,  0.064f, 1,
                    0.945f,  0.719f,  0.592f, 1,
                    0.543f,  0.021f,  0.978f, 1,
                    0.279f,  0.317f,  0.505f, 1,
                    0.167f,  0.620f,  0.077f, 1,
                    0.347f,  0.857f,  0.137f, 1,
                    0.055f,  0.953f,  0.042f, 1,
                    0.714f,  0.505f,  0.345f, 1,
                    0.783f,  0.290f,  0.734f, 1,
                    0.722f,  0.645f,  0.174f, 1,
                    0.302f,  0.455f,  0.848f, 1,
                    0.225f,  0.587f,  0.040f, 1,
                    0.517f,  0.713f,  0.338f, 1,
                    0.053f,  0.959f,  0.120f, 1,
                    0.393f,  0.621f,  0.362f, 1,
                    0.673f,  0.211f,  0.457f, 1,
                    0.820f,  0.883f,  0.371f, 1,
                    0.982f,  0.099f,  0.879f, 1
            },
            {
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,

                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,

                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,

                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,

                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,

                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f
            }, "..\\Shaders\\shader_base.vert",
                    "..\\Shaders\\shader_light.frag");
    DDDobject object2 (
            {
                    -0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f,  0.5f, -0.5f,
                    0.5f,  0.5f, -0.5f,
                    -0.5f,  0.5f, -0.5f,
                    -0.5f, -0.5f, -0.5f,

                    -0.5f, -0.5f,  0.5f,
                    0.5f, -0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f,  0.5f,
                    -0.5f, -0.5f,  0.5f,

                    -0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f, -0.5f,
                    -0.5f, -0.5f, -0.5f,
                    -0.5f, -0.5f, -0.5f,
                    -0.5f, -0.5f,  0.5f,
                    -0.5f,  0.5f,  0.5f,

                    0.5f,  0.5f,  0.5f,
                    0.5f,  0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,

                    -0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f, -0.5f,
                    0.5f, -0.5f,  0.5f,
                    0.5f, -0.5f,  0.5f,
                    -0.5f, -0.5f,  0.5f,
                    -0.5f, -0.5f, -0.5f,

                    -0.5f,  0.5f, -0.5f,
                    0.5f,  0.5f, -0.5f,
                    0.5f,  0.5f,  0.5f,
                    0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f,  0.5f,
                    -0.5f,  0.5f, -0.5f
            },
            {
                    0.583f,  0.771f,  0.014f, 1,
                    0.609f,  0.115f,  0.436f, 1,
                    0.327f,  0.483f,  0.844f, 1,
                    0.822f,  0.569f,  0.201f, 1,
                    0.435f,  0.602f,  0.223f, 1,
                    0.310f,  0.747f,  0.185f, 1,
                    0.597f,  0.770f,  0.761f, 1,
                    0.559f,  0.436f,  0.730f, 1,
                    0.359f,  0.583f,  0.152f, 1,
                    0.483f,  0.596f,  0.789f, 1,
                    0.559f,  0.861f,  0.639f, 1,
                    0.195f,  0.548f,  0.859f, 1,
                    0.014f,  0.184f,  0.576f, 1,
                    0.771f,  0.328f,  0.970f, 1,
                    0.406f,  0.615f,  0.116f, 1,
                    0.676f,  0.977f,  0.133f, 1,
                    0.971f,  0.572f,  0.833f, 1,
                    0.140f,  0.616f,  0.489f, 1,
                    0.997f,  0.513f,  0.064f, 1,
                    0.945f,  0.719f,  0.592f, 1,
                    0.543f,  0.021f,  0.978f, 1,
                    0.279f,  0.317f,  0.505f, 1,
                    0.167f,  0.620f,  0.077f, 1,
                    0.347f,  0.857f,  0.137f, 1,
                    0.055f,  0.953f,  0.042f, 1,
                    0.714f,  0.505f,  0.345f, 1,
                    0.783f,  0.290f,  0.734f, 1,
                    0.722f,  0.645f,  0.174f, 1,
                    0.302f,  0.455f,  0.848f, 1,
                    0.225f,  0.587f,  0.040f, 1,
                    0.517f,  0.713f,  0.338f, 1,
                    0.053f,  0.959f,  0.120f, 1,
                    0.393f,  0.621f,  0.362f, 1,
                    0.673f,  0.211f,  0.457f, 1,
                    0.820f,  0.883f,  0.371f, 1,
                    0.982f,  0.099f,  0.879f, 1
            },
            {
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,
                    0.0f,  0.0f, -1.0f,

                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,
                    0.0f,  0.0f, 1.0f,

                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,
                    -1.0f,  0.0f,  0.0f,

                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,
                    1.0f,  0.0f,  0.0f,

                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,
                    0.0f, -1.0f,  0.0f,

                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f,
                    0.0f,  1.0f,  0.0f
            }, "..\\Shaders\\shader_base.vert",
            "..\\Shaders\\shader_base.frag");
    DDDobject cube2=object2;

    object.modelMatrix = glm::mat4(1.0f);
    object.modelMatrix = glm::rotate(object.modelMatrix, glm::radians(-55.0f), glm::vec3(1.0f, 1.0f, 0.0f));
    object.modelMatrix = glm::translate(object.modelMatrix, glm::vec3(-3.0f, 0.0f, -10.0f));

    lightPosition = glm::vec4(0.0f, 0.0f, 0.0f,1) * object.modelMatrix;

    object2.modelMatrix = glm::mat4(1.0f);
    object2.modelMatrix = glm::rotate(object.modelMatrix, glm::radians(-95.0f), glm::vec3(1.0f, 1.0f, 0.0f));
    object2.modelMatrix = glm::translate(object.modelMatrix, glm::vec3(-2.0f, 0.0f, -5.0f));

    cube2.modelMatrix= glm::mat4(1.0f);
    cube2.modelMatrix = glm::translate(cube2.modelMatrix,glm::vec3(2.0f, 0.0f, -10.0f) );

    tdObjectsVector.push_back(std::move(object));
    tdObjectsVector.push_back(std::move(object2));
    tdObjectsVector.push_back(std::move(cube2));
}

void Model::update() {
    // Affichage des Frames par seconde (FPS)
    if (FramesData::Update()) {
        std::cerr << FramesData::getDescriptionFPS() << std::endl;
    }
    // Données d'environnement :
    niveauGris += 0.005f;
    if (niveauGris > 1.0f) {
        niveauGris = 0.0f;
    }
}

void Model::update_size(int height, int length) {
    this->height = height;
    this->length = length;
}

float Model::getNiveauGris() const {
    return niveauGris;
}

unsigned int * Model::getVao(){
    return vao;
}

const Shader &Model::getShader() const {
    return shader;
}

unsigned int Model::getVaoNumber() const {
    return vaoNumber;
}

unsigned int Model::getPointsNumber() const {
    return pointsNumber;
}

int Model::getHeight() const {
    return height;
}

int Model::getLength() const {
    return length;
}

std::vector<DDDobject> &Model::getTdObjectsVector()  {
    return tdObjectsVector;
}

const glm::vec4 &Model::getLightPosition() const {
    return lightPosition;
}

