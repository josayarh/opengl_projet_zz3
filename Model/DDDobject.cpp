//
// Created by jojoX on 19/12/2018.
//

#include <glad/glad.h>
#include <stb_image.h>
#include "DDDobject.hpp"

void DDDobject::bind3D() {
    glGenVertexArrays(1, &vao);
    glGenBuffers(3, vbo);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);

    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices.size(), &(vertices[0]), GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    if(!colors.empty()){
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*colors.size(), &(colors[0]), GL_DYNAMIC_DRAW);
        glVertexAttribPointer(1,4,GL_FLOAT, GL_FALSE, 0, (void*)0);
        glEnableVertexAttribArray(1);
    }

    if(!normals.empty()){
        glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*normals.size(), &(normals[0]), GL_DYNAMIC_DRAW);
        glVertexAttribPointer(2,3,GL_FLOAT, GL_FALSE, 0, (void*)0);
        glEnableVertexAttribArray(2);
    }

    glBindVertexArray(0);
}

DDDobject &DDDobject::operator=(const DDDobject &ddDobject) {
    vertices = ddDobject.vertices;
    colors = ddDobject.colors;
    normals = ddDobject.normals;
    bind3D();
}

void DDDobject::show() const{
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    glBindVertexArray(0);
}

const Shader &DDDobject::getShader() const {
    return shader;
}
