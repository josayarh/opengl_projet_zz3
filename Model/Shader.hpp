//
// Created by jojoX on 23/10/2018.
//

#ifndef PROJET_OPEN_GL_SHADER_HPP
#define PROJET_OPEN_GL_SHADER_HPP

#include <glm/glm.hpp>

class Shader
{
public:
    unsigned int ID;
    // constructor generates the shader on the fly
    // ------------------------------------------------------------------------
    Shader(const char* vertexPath, const char* fragmentPath);

    // activate the shader
    // ------------------------------------------------------------------------
    void use();
    // utility uniform functions
    // ------------------------------------------------------------------------
    void setBool(const std::string &name, bool value) const;
    // ------------------------------------------------------------------------
    void setInt(const std::string &name, int value) const;
    // ------------------------------------------------------------------------
    void setFloat(const std::string &name, float value) const;
    // ------------------------------------------------------------------------
    void setMat4(const std::string &name, glm::mat4 value)const;
    // ------------------------------------------------------------------------
    void setVec4(const std::string &name, glm::vec4 value) const;
    // ------------------------------------------------------------------------
    void setVec3(const std::string &name, glm::vec3 value) const;

private:
    // utility function for checking shader compilation/linking errors.
    // ------------------------------------------------------------------------
    void checkCompileErrors(unsigned int shader, std::string type);
};
#endif //PROJET_OPEN_GL_SHADER_HPP
