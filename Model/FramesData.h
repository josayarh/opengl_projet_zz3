#ifndef _FRAMESDATA_H
#define _FRAMESDATA_H


typedef unsigned uint;
typedef uint32_t u_int32_t;
typedef uint64_t u_int64_t;
//
//DONNÉES POUR LE TIMER ET LE CALCUL DES FRAMES PER SECONDS

class FramesData {
  private:
    // compteur de secondes
    static uint mNbSeconds;

    static u_int64_t mNbFrames;

    // compteur de frames
    static u_int64_t mLastNbFrames;

    // frames par secondes
    static u_int64_t mFps;

    static char mDescriptionFPS[200];


  public:
    static sf::Clock clock;

    // Initialisation de la description des FPS
    static void Init();

    //
    //Met à jour l'information du Frame Rate
    //@return true si une mise à jour a eu lieu (chaque seconde)
    
    static bool Update();

    //
    //@return description des numéro de frame, durée, et FPS
    
    static const char * getDescriptionFPS();

};
#endif
