//
// Created by jojoX on 19/10/2018.
//

#ifndef PROJET_OPEN_GL_MODEL_HPP
#define PROJET_OPEN_GL_MODEL_HPP
#include <memory>
#include <vector>

#include "FramesData.h"
#include "Camera.hpp"
#include "Shader.hpp"
#include "DDDobject.hpp"

class Model {
private:
    int height;
    int length;
    Shader shader;

    FramesData framesData;
    unsigned int vbo[2], vao[2], vaoNumber, pointsNumber;

    std::vector<DDDobject> tdObjectsVector;

    /** Niveau de gris du fond */
    float niveauGris;

    Camera camera;

    // On utilise un des points du premier cube et ca matrice de modèle pour calculer ce point
    glm::vec4 lightPosition;

    void makeTriangle();
    void makeCube();
    void makeCube2();

public:
    Model(int height, int length);
    void update();
    void update_size(int height, int length);

    float getNiveauGris() const;

    void useShader(){shader.use();}

    unsigned int * getVao() ;
    unsigned int getShaderProgram () const {return shader.ID;}
    const Shader &getShader() const;
    Camera &getCamera() {return camera;}

    unsigned int getVaoNumber() const;

    unsigned int getPointsNumber() const;

    int getHeight() const;

    int getLength() const;

    std::vector<DDDobject> &getTdObjectsVector();

    const glm::vec4 &getLightPosition() const;
};


#endif //PROJET_OPEN_GL_MODEL_HPP
