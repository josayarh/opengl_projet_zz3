
#include "FramesData.h"

// compteur de secondes
uint
::mNbSeconds= 0;

u_int64_t FramesData::mNbFrames=0;

// compteur de frames
u_int64_t FramesData::mLastNbFrames=0;

// frames par secondes
u_int64_t FramesData::mFps= 0;

char FramesData::mDescriptionFPS[200];

sf::Clock FramesData::clock;

// Initialisation de la description des FPS
void FramesData::Init()
{

    clock.restart();
	strcpy(mDescriptionFPS, "00 Frames en 00s, FPS: --");
}

//
//Met à jour l'information du Frame Rate
//@return true si une mise à jour a eu lieu (chaque seconde)

bool FramesData::Update()
{

    sf::Time frameTime = clock.restart();
    double framerate = 1 / (frameTime.asMilliseconds() * 0.001);
    std::cout << "Fps : " << framerate << std::endl;

	return false;
}

//
//@return description des numéro de frame, durée, et FPS

char FramesData::getDescriptionFPS()
{

    return mDescriptionFPS;
}

