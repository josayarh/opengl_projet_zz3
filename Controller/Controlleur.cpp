//
// Created by jojoX on 19/10/2018.
//
#include <View.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/OpenGL.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

#include "Controlleur.hpp"

Controlleur::Controlleur(sf::ContextSettings settings) : view(sf::VideoMode(800,600), "My window", settings), model(800,600)
{
    glMatrixMode(GL_PROJECTION);
    glm::mat4 projectionMatrix = glm::mat4(1.0f);
    projectionMatrix = glm::perspective(glm::radians(-55.0f), (float)model.getLength() / model.getHeight(), 0.1f, 1000.0f);
    glLoadMatrixf(&projectionMatrix[0][0]);
    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0f);
    glDepthFunc(GL_LEQUAL);
    glDepthMask(GL_TRUE);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

}


bool Controlleur::run() {
    bool is_open;

    is_open = handle_events();
    model.update();
    view.show(model);
    std::cout << glGetError() << std::endl;

    return is_open;
}

void Controlleur::handleKeyEvents(sf::Keyboard::Key key) {
    switch(key){
        case sf::Keyboard::Key::A:
            model.getCamera().moveLeft(0.5);
            break;
        case sf::Keyboard::Key::S:
            model.getCamera().moveBackward(0.5);
            break;
        case sf::Keyboard::Key::D:
            model.getCamera().moveRight(0.5);
            break;
        case sf::Keyboard::Key::W:
            model.getCamera().moveForward(0.5);
            break;
        default:
            break;
    }
}

void Controlleur::handleCameraMovement() {
    sf::Vector2i mousePosition = sf::Mouse::getPosition(view);
    model.getCamera().handleMouseMouvement(mousePosition);

}

bool Controlleur::handle_events() {
    sf::Event event = sf::Event();
    glm::mat4 projectionMatrix = glm::mat4(1.0f);

    while(view.pollEvent(event)) {
        switch (event.type) {
            case sf::Event::Resized :
                model.update_size(event.size.height, event.size.width);
                glViewport(0, 0, event.size.width, event.size.height);
                glMatrixMode(GL_PROJECTION);
                projectionMatrix = glm::perspective(glm::radians(-55.0f), (GLfloat)event.size.width / (GLfloat)event.size.height, 0.1f, 1000.0f);
                glLoadMatrixf(&projectionMatrix[0][0]);
                glMatrixMode(GL_MODELVIEW);
                break;
            case sf::Event::Closed :
                view.close();
                return false;
            case sf::Event::KeyPressed :
                handleKeyEvents(event.key.code);
                break;
            case sf::Event::MouseButtonReleased:
                if (event.mouseButton.button == sf::Mouse::Right){
                    model.getCamera().setIsFirstClick(true);
                }
                break;
            default:
                break;
        }
    }
    //On gère le boutton de la souris a part parce qu'on cherche a détécter un clic continu
    if(sf::Mouse::isButtonPressed(sf::Mouse::Right)){
        handleCameraMovement();
    }

    return true;
}
