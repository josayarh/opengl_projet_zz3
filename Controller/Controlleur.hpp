//
// Created by jojoX on 19/10/2018.
//

#ifndef PROJET_OPEN_GL_CONTROLLEUR_HPP
#define PROJET_OPEN_GL_CONTROLLEUR_HPP

#include <View.hpp>
#include <Model.hpp>

class Controlleur {
private:
    View view;
    Model model;

    bool handle_events();
    void handleKeyEvents(sf::Keyboard::Key key);
    void handleCameraMovement();
public:
    Controlleur(sf::ContextSettings settings);
    bool run();


};


#endif //PROJET_OPEN_GL_CONTROLLEUR_HPP
