//
// Created by jojoX on 19/10/2018.
//

#include <SFML/Graphics/Shader.hpp>
#include <glad/glad.h>
#include <Model.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "View.hpp"
#include <Model.hpp>
#include <iostream>
#include <cmath>

View::View(const sf::VideoMode &mode, const sf::String &title, sf::ContextSettings settings)
        : Window(mode, title, sf::Style::Titlebar | sf::Style::Resize | sf::Style::Close,settings)
{

    if(!gladLoadGL()) exit(1);
    glEnable(GL_TEXTURE_3D);
    setActive(true);

    setVerticalSyncEnabled(true);
}

void View::show(Model &model) {
    unsigned int* vao = model.getVao();

    glm::mat4 view          = glm::mat4(1.0f);
    glm::mat4 projection = glm::mat4(1.0f);

    model.getCamera().testUpdate();

    view  = model.getCamera().getView();
    projection = glm::perspective(glm::radians(90.0f),
            (float)model.getLength() / (float)model.getHeight(), 0.1f, 100.0f);

    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClearDepth(1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    /*for(int c = 0; c<model.getVaoNumber(); c++){
        glBindVertexArray(vao[c]);
        glDrawArrays(GL_TRIANGLES, 0, model.getPointsNumber());
    }*/

    for(DDDobject &obj : model.getTdObjectsVector()){
        obj.useShader();

        obj.getShader().setMat4("view",view);
        obj.getShader().setMat4("projection",projection);
        obj.getShader().setMat4("model",obj.modelMatrix);
        obj.getShader().setVec4("lightColor",glm::vec4(1,1,1,1));
        obj.getShader().setVec4("lightPos",model.getLightPosition());
        obj.getShader().setVec3("viewPos",model.getCamera().getPosition());
        obj.show();
    }

    model.getCamera().testUpdate();

    /*int vertexColorLocation = glGetUniformLocation(model.getShaderProgram(), "ourColor");
    glUniform4f(vertexColorLocation, 0.0f, model.getNiveauGris(), 0.0f, 1.0f);*/

    sf::Window::display();
}
