//
// Created by jojoX on 19/10/2018.
//

#ifndef PROJET_OPEN_GL_VIEW_HPP
#define PROJET_OPEN_GL_VIEW_HPP

#include <SFML/Window.hpp>
//#include <SFML/Window.hpp>

class Model;
class View : public sf::Window {
private:
    unsigned int shaderProgram;
public:
    View(const sf::VideoMode &mode, const sf::String &title,sf::ContextSettings settings);
    void show(Model &model);
};


#endif //PROJET_OPEN_GL_VIEW_HPP
